module.exports = {
  csv: {
    sheet1: {
      base: {
        protocol: 'https',
        hostname: 'docs.google.com',
        pathname:
          'spreadsheets/d/1hEza8F4QySnpd1alM2EudUn_V8GghmgbwrsgLXxUY_s/export',
      },
      sheets: {
        table1: { gid: 0, caption: 'First table' },
      },
    },
  },
  svg: {
    sheetB: {
      base: {
        protocol: 'https',
        hostname: 'docs.google.com',
        pathname:
          '/spreadsheets/d/1hEza8F4QySnpd1alM2EudUn_V8GghmgbwrsgLXxUY_s/embed/oimg',
      },
      sheets: {
        graph1: 1797723469,
      },
    },
  },
}
