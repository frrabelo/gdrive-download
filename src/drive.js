const url = require('url')
const path = require('path')
const { https } = require('follow-redirects')
const fs = require('fs')

const csv = require('csv-parser')
const config = require('./config.js')

function mdRow(array) {
  return (
    '|' + array.map((value) => value.replace(' €', '&nbsp;€')).join('|') + '|\n'
  )
}

const genFolder = path.join('/dist')
if (!fs.existsSync(genFolder)) {
  fs.mkdirSync(genFolder)
}

// Csv
for (let csvFile of Object.values(config.csv)) {
  for (let [name, table] of Object.entries(csvFile.sheets)) {
    let file = fs.createWriteStream(path.join(genFolder, name + '.md'))
    if (table.caption) {
      file.write('Table: ' + table.caption + '\n\n')
    }
    let colIdx = false
    https.get(
      url.format({ ...csvFile.base, query: { format: 'csv', gid: table.gid } }),
      (response) =>
        response
          .pipe(csv({ escape: '"', headers: false }))
          .on('data', (data) => {
            if (!colIdx) {
              colIdx = Object.keys(data)
              file.write(mdRow(colIdx.map((idx) => data[idx])))
              file.write(
                mdRow([':---', ...Array(colIdx.length - 1).fill('---:')])
              )
            } else {
              file.write(mdRow(colIdx.map((idx) => data[idx])))
            }
          })
          .on('end', file.end)
    )
  }
}

const SVG_QUERY = {
  disposition: 'ATTACHMENT',
  bo: 'false',
  filetype: 'svg',
}

for (let svgFile of Object.values(config.svg)) {
  for (let [name, oid] of Object.entries(svgFile.sheets)) {
    let file = fs.createWriteStream(path.join(genFolder, name + '.svg'))
    https.get(
      url.format({ ...svgFile.base, query: { ...SVG_QUERY, oid } }),
      (response) => response.pipe(file)
    )
  }
}
