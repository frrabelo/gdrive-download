# GDrive Download

Download helper for google sheets / graph document

## Usage

```console
docker run --rm -it \
		-v "/exemple.config.js:/app/config.js" \
		-v "$(pwd)/dist/:/dist" \
		-u "$(id -u):$(id -g)" \
		savadenn/gdrive-dowload
```
